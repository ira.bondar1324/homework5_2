import React from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";


const Modal = ({changeCountElement,el,header,text,isOpen,changeStateModal, changeCountCart}) =>{

    let displayIs;
    isOpen?displayIs={display:"flex"}:displayIs={display:"none"};

    const click = (e)=>{
        if(e.target.className==="Modal_modalWindowHeaderCross__4CXpR"||e.target.className==="Modal_modalWrapper__cn1IW"){
            changeStateModal();
        }
    }

    return(
        <div onClick={click} style={displayIs} className={styles.modalWrapper}>
            <div className={styles.modalWindow}>
                <header className={styles.modalWindowHeader}>
                    <h2 className={styles.modalWindowHeaderText}>{header}</h2>
                    <p className={styles.modalWindowHeaderCross}>X</p>
                </header>
                <main className={styles.modalWindowMain}>
                    <p className={styles.modalWindowMainText}>{text}</p>
                    <div className={styles.modalMainBtns}>
                        <button onClick={()=>{changeCountCart();changeStateModal();changeCountElement(el)}}  className={styles.modalBtn}>Yes</button>
                        <button onClick={changeStateModal} className={styles.modalBtn}>No</button>
                    </div>
                </main>
            </div>
        </div>
    );

}

Modal.propTypes={
    changeCountElement:PropTypes.func,
    el:PropTypes.object,
    header:PropTypes.string,
    text:PropTypes.string,
    isOpen:PropTypes.bool,
    changeStateModal:PropTypes.func,
    changeCountCart:PropTypes.func
}

Modal.defaultProps={
    changeCountElement:()=>{},
    el:{},
    header:"",
    text:"",
    isOpen:false,
    changeStateModal:()=>{},
    changeCountCart:()=>{}
}

export default Modal;