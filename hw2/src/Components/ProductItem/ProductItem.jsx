import React,{useState} from "react";
import styles from "./ProductItem.module.scss";
import Button from "../Button/Button.jsx";
import {ReactComponent as HeartIconPlus} from "../../img/heartIconRemove.svg";
import PropTypes from "prop-types";

const ProductItem=({favoritesOfEl,changeEl,el,changeFavoriteOfElement,changeStateModal})=>{

const [colorHeart,setColorHeart]=useState(false);

const changeColorHeart=()=>{
    setColorHeart((prev)=>{
        let color=!prev;
        return color;
    })
}


return(
    <li className={styles.productItem}>
        <img className={styles.productItemImg} src={el.url} alt="" />
        <div className={styles.productItemInfo}>
            <h3 className={styles.productItemName}>{el.name}</h3>
            <h4 className={styles.productItemColor}>Color: {el.color}</h4>
            <h4 className={styles.productItemPrice}>{el.price} $</h4>
        </div>
        <div className={styles.productItemBtns}>
            <Button  favoritesOfEl={favoritesOfEl} changeEl={changeEl} changeColorHeart={changeColorHeart} openBtn="favorite" changeFavoriteOfElement={changeFavoriteOfElement} el={el} 
            backgroundColor="white">
                <HeartIconPlus className={el.favorite?styles.heartBtnTrue:styles.heartBtnFalse}/>
            </Button>
            <Button el={el}  changeEl={changeEl} openBtn="modal" changeStateModal={changeStateModal} backgroundColor="black">Add to cart</Button>
        </div>
    </li>
);
}

ProductItem.propTypes={
    favoritesOfEl:PropTypes.array,
    changeEl:PropTypes.func,
    el:PropTypes.object,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func
}

ProductItem.defaultProps={
    favoritesOfEl:[],
    changeEl:()=>{},
    el:{},
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{}
}

export default ProductItem;