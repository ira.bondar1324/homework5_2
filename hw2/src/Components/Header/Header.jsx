import React from "react";
import {ReactComponent as HeartIconPlus} from "../../img/heartIconRemove.svg";
import {ReactComponent as CartIcon} from "../../img/cartIcon.svg";
import styles from "./Header.module.scss";
import PropTypes from "prop-types";

const Header=({countFavorite, countCart})=>{
return(
    <div className={styles.headerWrapper}>  

        <img src="https://timeshop.com.ua/images/ab__webp/logos/169/timeshop-logo-1_png.webp" alt="Timeshop" />
        <div className={styles.headerWrapperIcons}>

            <div className={styles.headerWrapperIconsItem}>
                <HeartIconPlus className={styles.headerIcon}/>
                <p className={styles.headerIconText}>{countFavorite}</p>
            </div>

            <div className={styles.headerWrapperIconsItem}>
                <CartIcon className={styles.headerIcon}/>
                <p className={styles.headerIconText}>{countCart}</p>
            </div>
        </div>
    </div>
);
}

Header.propTypes={
    countFavorite:PropTypes.number,
    countCart:PropTypes.number
}

Header.defaultProps={
    countFavorite:0,
    countCart:0
}

export default Header;