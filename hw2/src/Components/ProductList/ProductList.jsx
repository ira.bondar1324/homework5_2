import React from "react";
import ProductItem from "../ProductItem/ProductItem";
import styles from "./ProductList.module.scss";
import PropTypes from "prop-types";

const ProductList=({favoritesOfEl,changeEl,productList,changeFavoriteOfElement,changeStateModal})=>{
return(
    <ul className={styles.productList}>
        {productList.map(el=><ProductItem favoritesOfEl={favoritesOfEl} changeEl={changeEl} changeStateModal={changeStateModal} changeFavoriteOfElement={changeFavoriteOfElement} key={el.id} el={el}/>)}
    </ul>
);
}

ProductList.propTypes={
    favoritesOfEl:PropTypes.array,
    changeEl:PropTypes.func,
    productList:PropTypes.array.isRequired,
    changeFavoriteOfElement:PropTypes.func,
    changeStateModal:PropTypes.func
}

ProductList.defaultProps={
    favoritesOfEl:[],
    changeEl:()=>{},
    productList:[],
    changeFavoriteOfElement:()=>{},
    changeStateModal:()=>{}
}

export default ProductList;