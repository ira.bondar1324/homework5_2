import './App.css';
import Header from './Components/Header/Header';
import ProductList from './Components/ProductList/ProductList';
import React, { useEffect, useState } from 'react';
import Modal from "./Components/Modal/Modal.jsx";
import { saveStateToLocalStorage, getStateFromLocalStorage } from './utils/localStorageHelper';
import {CART_LS_KEY} from './constants';

function App() {

 const [productList, setProductList]=useState([]);
  const [countFavorite,setCountFavorite]=useState(0);
  const [modal,setModalState]=useState(false);
  const [countCart,setCountCart]=useState(0);
  const [el,setEl]=useState(null);
  const [favoritesEl,setFavoritesEl]=useState([]);

  const favoritesOfEl=(el)=>{
    let favorArr;
    const findEl=favoritesEl.findIndex(elm=>el.id===elm.id);
    console.log(findEl);
    if(findEl===-1){
      setFavoritesEl((prev)=>{
        favorArr=[...prev];
        favorArr.push(el);
        saveStateToLocalStorage('favoritesEl',favorArr)
        return favorArr;
      })
    }else{
      console.log(findEl);
      setFavoritesEl((prev)=>{
        favorArr=[...prev];
        favorArr.splice(findEl,1);
        saveStateToLocalStorage('favoritesEl',favorArr)
        return favorArr;
      })
    }
  }


  const changeCountElement=(el)=>{
    el.isAddToCart=el.isAddToCart+1;
    saveStateToLocalStorage(CART_LS_KEY,productList);
  }

   const changeEl=(el)=>{
    console.log(el);
    setEl(el);
  }

  function changeStateModal(){
      if(modal){
        setModalState(false)
      }else{
        setModalState(true)
      }
  }

  const changeCountCart=()=>{
    setCountCart((prev)=>{
      let count=prev;
      count++;
      saveStateToLocalStorage('countCart',count);
      return count;
    })
  }

  const changeFavoriteOfElement=(el)=>{
    let counter;
    if(el.favorite){
      el.favorite=false;
      setCountFavorite((prev)=>{
        counter=prev;
        counter--;
        saveStateToLocalStorage('countFavorite',counter);
        return counter;
      })
    }else{
      el.favorite=true;
      setCountFavorite((prev)=>{
        counter=prev;
        counter++;
        saveStateToLocalStorage('countFavorite',counter);
        return counter;
      })
    }
    saveStateToLocalStorage(CART_LS_KEY,productList);
  }


  const getItems = async()=>{
    try{
      const data=await fetch("./products.json").then(res=>res.json());
      setProductList(data.products);
      saveStateToLocalStorage(CART_LS_KEY,data.products)
    }catch(e){
      console.log(e.message);
    }
  }

  useEffect(()=>{
    const cartFromLs=getStateFromLocalStorage(CART_LS_KEY);
    if(cartFromLs){
      setProductList(cartFromLs);
    }else{
      getItems();
    }

    const countFavoriteEl =getStateFromLocalStorage("countFavorite");
    if(countFavoriteEl){
      setCountFavorite(countFavoriteEl);
    }

    const countCartEl =getStateFromLocalStorage("countCart");
    if(countCartEl){
      setCountCart(countCartEl);
    }

    const favoritesElements=getStateFromLocalStorage('favoritesEl');
    if(favoritesElements){
      setFavoritesEl(favoritesElements);
    }
  },[])

  return (
    <>
      <Header countFavorite={countFavorite} countCart={countCart}/>
      <ProductList favoritesEl={favoritesEl} favoritesOfEl={favoritesOfEl} changeEl={changeEl} changeStateModal={changeStateModal} changeFavoriteOfElement={changeFavoriteOfElement} productList={productList} />
      <Modal el={el} changeCountElement={changeCountElement} changeCountCart={changeCountCart} changeStateModal={changeStateModal} header="Add product to cart" text="Do you want to add this product is cart?" isOpen={modal}/>
    </>
  );
}

export default App;
